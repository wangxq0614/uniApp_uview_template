import {
	http,
	upload
} from "@/api/request.js";



/**
 * 文件上传
 * @param {String} url 文件地址
 */
export const uploadFiles = (url) => {
	return upload({
		url: `/common/common/upload/file`,
		data: {
			url
		}
	});
}

/**
 * 获取省市区
 * @param {String | Number} pid 地区id
 */
export const getCityDataByPid = (pid) => {
	return http({
		url: `/p/area/listByPid`,
		method: "get",
		data:{
			pid
		}
	});
}