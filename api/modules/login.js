import {
	http
} from "@/api/request.js";

/**
 * 小程序登录
 * @param { String } code
 * @param { String } phoneCode
 */
export const weappLogin = (data) =>
	http({
		url: ``,
		method: "post",
		data,
	});
	
/**
 * 短信登录 * 
 * @param { String } code
 * @param { String } mobile
 */
export const smsLogin = (data) =>
	http({
		url: ``,
		method: "post",
		data,
	});
/**
 * 获取用户信息
 */
export const getUserInfo = (data) =>
	http({
		url: ``,
		method: "get",
		data,
	});
/**
 * 发送验证码 
 */
export const sendSmsCode = (data) =>
	http({
		url: ``,
		method: "get",
	});

/**
 * 刷新token
 */
export const getRefreshToken = (data) =>
	http({
		url: ``,
		method: "get",
	});