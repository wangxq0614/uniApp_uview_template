import store from "@/store/index.js";
import { BASE_URL, handleRequestPath } from "@/config/index.js";
import { getRefreshToken } from "@/api/modules/login.js";

let loadingCount = 0;
let isLoading = true;
let retryCount = 1;
let startTime = 0;
let refreshTokenPromise = null;

const showLoading = (options) => {
  startTime = Date.now();
  isLoading = options.loading !== undefined ? options.loading : true;
  if (!isLoading) return;
  loadingCount++;
  if (loadingCount === 1) {
    uni.showLoading({
      title: "加载中",
      mask: true,
    });
  }
};

const hideLoading = () => {
  loadingCount > 0 && loadingCount--;
  if (loadingCount === 0) {
    uni.hideLoading();
  }
};

const handleError = (message) => {
  loadingCount = 0;
  if (isLoading) {
    uni.hideLoading();
    isLoading = false;
  }
  uni.showToast({
    title: message || "网络错误",
    icon: "none",
  });
};

const handleRefreshToken = async () => {
  if (!refreshTokenPromise) {
    refreshTokenPromise = new Promise(async (resolve, reject) => {
      const { refreshToken } = store.getters.token;
      try {
        const data = await getRefreshToken(refreshToken);
        await store.dispatch("user/saveToken", data);
        resolve();
      } catch (e) {
        store.dispatch("user/loginOut");
        console.error("获取新token失败", e);
        uni.navigateTo({
          url: "/pages/login/login",
        });
        reject();
      } finally {
        refreshTokenPromise = null;
      }
    });
  }
  return refreshTokenPromise;
};

const handleResponse = async (res, options) => {
  const { code, data, result, status, msg, message } = res.data;
  loadingCount && hideLoading();
  if (code === 200 || code == '00000') {
    return result || data;
  } else if (status === 401 || status === 403) {
    if (retryCount > 0) {
      retryCount--;
      await handleRefreshToken(options);
      return handleRequest(options);
    } else {
      handleError(message || msg || res.statusCode + "，请联系客服处理");
      throw new Error(message || msg || res.statusCode + "，请联系客服处理");
    }
  } else {
    handleError(message || msg || res.statusCode + "，请联系客服处理");
    throw new Error(message|| msg  || res.statusCode + "，请联系客服处理");
  }
};

const handleRequest = async (options) => {
  const { accessToken } = store.getters.token;
  handleRequestPath(options);
  if (Date.now() - startTime > 1000) {
    showLoading(options);
  }
  return new Promise((resolve, reject) => {
    uni.request({
      url: BASE_URL + options.url,
      method: options.method,
      data: options.data,
      header: {
        uuid: Date.now(),
        // accessToken: accessToken || "",
        Authorization:  'cd3201ec-f96e-40fc-b5f2-3f1ddccc08b1' || "",
        "content-type": "application/json",
      },
      success: (res) => {
        resolve(handleResponse(res, options));
      },
      fail: (err) => {
        console.error(err);
        reject(handleError("网络错误"));
      },
    });
  });
};

/* 文件上传 */
const handleUploadRequest = (options) => {
  const { accessToken } = store.getters.token;
  handleRequestPath(options);
  if (Date.now() - startTime > 500) {
    showLoading(options);
  }
  return new Promise((resolve, reject) => {
    uni.uploadFile({
      url: BASE_URL + options.url,
      filePath: options.data.url,
      name: "file",
      header: {
        accessToken: accessToken || "",
      },
      success: (res) => {
        const data = {
          ...res,
          data: JSON.parse(res.data),
        };
        resolve(handleResponse(data, options));
      },
      fail: (err) => {
        console.error(err);
        reject(handleError("网络错误"));
      },
    });
  });
};

export { handleRequest as http, handleUploadRequest as upload };
