# uniApp  + uview

#### 【uview2.0 + vue2 + vuex】

#### 介绍
uniapp + uview模板

#### 安装教程

git clone https://gitee.com/wangxq0614/uniApp_uview_template.git

cd uniApp_uview_template

npm install

HBuilder运行

#### 仓库地址

##### 码云  https://gitee.com/wangxq0614/uniApp_uview_template.git

#### 使用说明

开箱即用（搭配 uview官方教程更配哦）

#### 感谢

1. [uview](https://www.uviewui.com/)
2. [uni-app](https://uniapp.dcloud.net.cn/)
