import Vue from "vue";
import App from "./App";
import store from "./store";
import dayjs from "dayjs";
import uView from "uview-ui";
import * as utils from "@/utils";

Vue.use(uView);

Vue.prototype.$dayjs = dayjs;
Vue.prototype.$utils = utils;

Vue.config.productionTip = false;

App.mpType = "app";

const app = new Vue({
  store,
  ...App,
});

app.$mount();
