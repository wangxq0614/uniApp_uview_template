const SET_TOKEN = "SET_TOKEN";
const SET_USER_INFO = "SET_USER_INFO";
import { getUserInfo } from "@/api/modules/login.js";

export default {
  namespaced: true,

  state: () => {
    return {
      token: uni.getStorageSync("token") || {},
      userInfo: uni.getStorageSync("userInfo") || {},
    };
  },

  mutations: {
    [SET_TOKEN](state, data) {
      state.token = data;
    },
    [SET_USER_INFO](state, data) {
      state.userInfo = data;
    },
  },

  actions: {
    // 保存token
    async saveToken({ commit, dispatch }, data) {
      try {
        uni.setStorageSync("token", data);
        commit(SET_TOKEN, data);
        await dispatch("saveUserInfo");
      } catch (error) {
        throw new Error("保存token失败" + JSON.stringify(error));
      }
    },
    // 保存用户信息
    async saveUserInfo({ commit }) {
      try {
        const data = await getUserInfo();
        uni.setStorageSync("userInfo", data);
        commit(SET_USER_INFO, data);
        return data;
      } catch (e) {
        uni.removeStorageSync("userInfo");
        commit(SET_USER_INFO, {});
        throw new Error(JSON.stringify(e));
      }
    },
    // 退出登录
    loginOut({ commit }) {
      uni.clearStorageSync();
      commit(SET_USER_INFO, {});
      commit(SET_TOKEN, "");
    },
  },
};
