const SET_LOCATION = "SET_LOCATION";

export default {
  namespaced: true,
  state: () => {
    return {
      location: uni.getStorageSync("location") || {},
    };
  },

  mutations: {
    [SET_LOCATION](state, data) {
      state.location = data;
    },
  },

  actions: {
    /**
     * 保存位置信息
     * @param {Object} data 位置对象
     * @param {String | Number} data.code 城市编号
     * @param {String | Number} data.lat	纬度
     * @param {String | Number} data.lng 经度
     * @param {String} data.name 城市名
     * @param {String} data.province 省
     * @param {String | Number} data.provinceCode 省市编码
     * @param {Boolean} data.isDef 是否默认地址 false
     * @param {Object} data.defLocation 默认地址信息
     */
    saveLocation({ commit }, data) {
      uni.setStorageSync("location", data);
      commit(SET_LOCATION, data);
    },
  },
};
