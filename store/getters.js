export default {
  userInfo: (state) => state.user.userInfo,
  token: (state) => state.user.token,

  location: (state) => state.app.location,
};
