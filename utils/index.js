import permision from "@/utils/sdk/permission.js";
import { BASE_URL } from "@/config/index.js";
/**
 * 字符串图片转数组
 * @param {String} 字符串
 */
export function initImgs(str) {
  return str ? str.split(",") : [];
}
/**
 * 保留小数位
 * @param {String} val 需要操作的字符串
 * @param {Number} len 长度
 */
export function formatDecimal(val, len = 2) {
  if (!val) return [0, 0];
  let valNum = new Number(val);
  return valNum.toFixed(len).split(".");
}

/**
 * 数据脱敏
 * @param {String} sensitiveInfo 需要脱敏的字符串
 * @param {Number} start 开始位置
 * @param {Number} end 结束位置
 */
export function maskSensitiveInfo(sensitiveInfo, start = 1, end) {
  if (!sensitiveInfo) return "";
  const len = sensitiveInfo.length;
  const maskLen = end ? end - start : 2;
  const maskStr = "*".repeat(maskLen);
  return sensitiveInfo.slice(0, start) + maskStr + sensitiveInfo.slice(end || len - 1, len);
}

/**
 * 大图预览
 * @param {Array} urls 图片数组对象
 * @param {String} current 当前点击图片
 */
export function showBigImg(urls, current) {
  uni.previewImage({
    urls: typeof urls === "object" ? urls : urls.split(","),
    current,
  });
}

/**
 * 页面跳转
 * @param {String} path 跳转路径
 * @param {Boolean} isLogin false
 * @param {String} type navigateTo 跳转类型
 * @param {Function} callBack 事件回调
 */
export function pageGo(path, isLogin = false, type = "navigateTo", callBack = null) {
  const token = uni.getStorageSync("token");
  if (isLogin && !token) {
    uni.navigateTo({
      url: "/pages/login/login?path=" + path,
    });
    return;
  }
  uni[type]({
    url: path,
  });
  return callBack && callBack();
}
/**
 * 获取url参数
 * @param {String} name 参数名
 */
export function getUrlParams(name) {
  return (
    decodeURIComponent(
      (new RegExp("[?|&]" + name + "=" + "([^&;]+?)(&|#|;|$)").exec(location.href) || [
        ,
        "",
      ])[1].replace(/\+/g, "%20")
    ) || null
  );
}

/**
 * 检查用户授权权限（安卓）
 * @param {String} permisionID 权限id
 */
export function requestAndroidPermission() {
  return new Promise(async (resolve, reject) => {
    const sys = uni.getSystemInfoSync();
    if (sys.osName === "android" && sys.uniPlatform === "app") {
      const result = await permision.requestAndroidPermission(permisionID);
      if (result == 1) {
        // strStatus = "已获得授权"
        resolve(true);
      } else if (result == 0) {
        // strStatus = "未获得授权"
        commit(SET_IS_SHOW_SET_MODEL, true);
        reject(true);
      } else {
        commit(SET_IS_SHOW_SET_MODEL, true);
        reject(false);
        // strStatus = "被永久拒绝权限"
      }
    } else {
      resolve(true);
    }
  });
}
/**
 * 补充图片路径
 * @param {String} src
 */
export function assembleImgSrc(src) {
  if (!src) return "/static/logo.png";
  if (src.indexOf("http://") === 0 || src.indexOf("https://") === 0) {
    return src;
  }
  return BASE_URL + src;
}
/**
 * 格式化距离
 * @param {Number} number 数值
 */
export function formatDistancer(number) {
  number = Number(number);
  return number >= 1000 ? (number / 1000).toFixed(1) + "km" : number.toFixed(1) + "m";
}
